# https://docs.python.org/es/3/tutorial/floatingpoint.html#representation-error

from decimal import Decimal
from fractions import Fraction

print(2**52 <= 2 **56 // 10 < 2**53)

print()

q, r = divmod(2**56, 10)
print(r)
print(r+1)

print()

print(0.1 * 2 ** 55)

print()

print(3602879701896397 * 10 ** 55 // 2 ** 55)

print()

print(format(0.1, '.17f'))

print()

print(Fraction.from_float(0.1))
print((0.1).as_integer_ratio())
print(Decimal.from_float(0.1))
print(format(Decimal.from_float(0.1), '.17'))
