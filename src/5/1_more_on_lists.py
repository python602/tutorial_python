# https://docs.python.org/3/tutorial/datastructures.html#more-on-lists

fruits = ['orange', 'apple', 'pear', 'banana', 'kiwi', 'apple', 'banana']
print(fruits.count('apple'))
print(fruits.count('tangerine'))

print()

print(fruits.index('banana'))
print(fruits.index('banana', 4)) # Find next banana starting a position 4

print()

fruits.reverse()
print(fruits)

fruits.append('grape')
print(fruits)

fruits.sort()
print(fruits)

print()

print(fruits.pop())
