# https://docs.python.org/3/tutorial/datastructures.html#tuples-and-sequences

t = 12345, 54321, 'hello!'
print(t[0])
print(t)

print()

# Tuples may be nested:
u = t, (1, 2, 3, 4, 5)
print(u)

print()

# Tuples are immutable:
# t[0] = 8888

# but they can contain mutable objects:
v = ([1, 2, 3], [3, 2, 1])
print(v)

print()

empty = ()
singleton = 'hello', # <-- note trailing comma
print(len(empty))
print(len(singleton))
print(singleton)

print()

x, y, z = t

print(x)
print(y)
print(z)
