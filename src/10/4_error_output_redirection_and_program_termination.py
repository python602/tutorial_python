# https://docs.python.org/es/3/tutorial/stdlib.html#error-output-redirection-and-program-termination

import sys

sys.stderr.write('Warning, log file not found starting a new one\n')

# sys.exit()  # Direct way to finish a program
