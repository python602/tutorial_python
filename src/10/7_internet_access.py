# https://docs.python.org/es/3/tutorial/stdlib.html#internet-access

from urllib.request import urlopen
import smtplib

with urlopen('http://worldtimeapi.org/api/timezone/etc/UTC.txt') as response:
    for line in response:
        line = line.decode()        # Convert bytes to a str
        if line.startswith('datetime'):
            print(line.rstrip())    # Remove trailing newline

server = smtplib.SMTP('localhost')
server.sendmail('soothsayer@example.org', 'jcaesar@example.org',
    """To: jcaesar@example.org
    From: soothsayer@example.org

    Beware the Ides of March.
    """)
server.quit()
