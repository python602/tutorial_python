# https://docs.python.org/es/3/tutorial/stdlib.html#string-pattern-matching

import re

print(re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest'))
print(re.sub(r'(\b[a-z]+) \1', r'\1', 'cat in the the hat'))

print()

print('tea for too'.replace('too', 'two'))
