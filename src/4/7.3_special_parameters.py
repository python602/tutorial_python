# https://docs.python.org/3/tutorial/controlflow.html#special-parameters

def standar_arg(arg):
    print(arg)

def pos_only_arg(arg, /):
    print(arg)

def kwd_only_arg(*, arg):
    print(arg)

def combined_example(pos_only, /, standard, *, kwd_only):
    print(pos_only, standard, kwd_only)

standar_arg(2)
standar_arg(arg=2)

print()

pos_only_arg(1)
# pos_only_arg(arg=1)

print()

# kwd_only_arg(3)
kwd_only_arg(arg=3)

print()

# combined_example(1, 2, 3)
combined_example(1, 2, kwd_only=3)
combined_example(1, standard=2, kwd_only=3)
# combined_example(pos_only=1, standard=2, kwd_only=3)

def foo(name, **kwds):
    return 'name' in kwds

print()

# foo(1, **{'name': 2})

def foo(name, /, **kwds):
    return 'name' in kwds

print(foo(1, **{'name': 2}))

def f (pos1, pos2, /, pos_or_wd, *, kwd1, kwd2):
    pass
