# https://docs.python.org/es/3/tutorial/classes.html#multiple-inheritance

class DerivedClassName(Base1, Base2, Base3):
    '''
    <statement - 1>
    .
    .
    .
    <statement - N>
    '''
