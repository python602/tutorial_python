# https://docs.python.org/es/3/tutorial/classes.html#class-objects

class MyClass:
    """A simple example class"""
    i = 12345

    def f(self):
        return 'Hello world'

x = MyClass()

class Complex:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart

x = Complex(3.0, -4.5)
print(x.r, x.i)
