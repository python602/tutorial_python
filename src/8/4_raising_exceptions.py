# https://docs.python.org/es/3/tutorial/errors.html#raising-exceptions

raise NameError('HiThere')

raise ValueError # shorthand for

try:
    raise NameError('HiThere')
except NameError:
    print('An exception flew by!')
    raise
