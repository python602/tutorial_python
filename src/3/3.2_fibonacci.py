# https://docs.python.org/3/tutorial/introduction.html#first-steps-towards-programming

# Fibonacci series:
# the sum of two elements defines the next
a, b = 0, 1
while a < 10:
    print(a)
    a, b = b, a + b

print('')

i = 256 * 256
print('The value of i is', i)

print('')

a, b = 0, 1
while a < 1000:
    print(a, end=', ')
    a, b = b, a + b

print('')

print(-3 ** 2)

print((-3) ** 2)
