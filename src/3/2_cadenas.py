#https://docs.python.org/3/tutorial/introduction.html#strings

print("1)", 'spam eggs') # single quotes

print("2)", 'doesn\'t') # use \' to escape the single qoute...

print("3)", "doesn't") # ...or use double quotes instead

print("4)", '"Yes," they said.')

print("5)", "\"Yes,\" they said.")

print("6)", '"Isn\'t," they said.')

s = 'First line.\nSecond line.' # \n means newline
print("7)", s)

print("8)", 'C:\some\name') # here \n mans newline

print("9)", r'C:\some\name') # note the r before the quote

print("10)", """\
Usage: thingy [OPTIONS]
    -h                      Display this usage message
    -H hostname             Hostname to connect to
""")

# 3 times 'un', followed by 'ium'
print("11)", 3 * 'un' + 'ium')

print("12)", 'Py' 'thon')

text = ('Put several strings within parentheses '
        'to have them joined together')
print("13)", text)

prefix = 'Py'
print("14)", prefix + 'thon')

word = 'Python'
print("15)", word[0]) # character in position 0

print("16)", word[5]) # character in position 5

print("17)", word[-1]) # last character

print("18)", word[-2]) # second-last character

print("19)", word[-6])

print("20)", word[0:2]) # characters from position 0 (included) to 2 (excluded)

print("21)", word[2:5]) # characters from position 2 (included) to 5 (excluded)

print("22)", word[:2] + word[2:])

print("23)", word[:4] + word[4:])

print("24)", word[:2]) # character from the beginning to position 2 (excluded)

print("25)", word[4:]) # characters from position 4 (included) to the end

print("26)", word[-2:]) # characters from the second-last (included) to the end

print("27)", word[4:42])

print("28)", word[42:])

print("29)", 'J' + word[1:])

print("30)", word[:2] + 'py')

s = 'supercalifragilisticexpialidocious'
print("31)", len(s))
