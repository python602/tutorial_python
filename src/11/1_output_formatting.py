# https://docs.python.org/es/3/tutorial/stdlib2.html#output-formatting

import reprlib
import pprint
import textwrap
import locale

print(reprlib.repr(set('supercalifragilisticexpialidocious')))

print()

t = [[[['black', 'cyan'], 'white', ['green', 'red']], [['magenta', 'yellow'], 'blue']]]
pprint.pprint(t, width=30)

print()

doc = """The wrap() method is just like fill() except that it returns a list of strings instead of one big string with newlines to separate the wrapped lines."""
print(textwrap.fill(doc, width=40))

print()

print(locale.setlocale(locale.LC_ALL, 'English_United States.1252'))

print()

conv = locale.localeconv()          # get a mapping of conventions
x = 1234567.8
print(locale.format("%d", x, grouping=True))

print(locale.format_string("%s%.*f", (conv['currency_symbol'], conv['frac_digits'], x), grouping=True))
