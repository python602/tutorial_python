# https://docs.python.org/es/3/tutorial/inputoutput.html#old-string-formatting

import math

print('The value of pi is approximately %5.3f.' % math.pi)
