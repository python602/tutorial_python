# https://docs.python.org/es/3/tutorial/inputoutput.html#saving-structured-data-with-json

import json

x = [1, 'simple', 'list']
print(json.dumps(x))
