# https://docs.python.org/es/3/tutorial/inputoutput.html#reading-and-writing-files

f = open('workfile', 'w')

with open('workfile') as f:
    read_data = f.read()

# We can check that the file has been automatically closed.
print(f.closed)

f.close()
f.read()
